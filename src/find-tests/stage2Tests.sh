#!/bin/bash

let failedTests=0
testsFolder=/tmp/find${RANDOM}
findResultFile=/tmp/findResult
customeFindResultFile=/tmp/customeFindResult
# Change it according to your find program
customeFindPath=find

function PrintError
{
    echo "----------------------"
    echo "fail on test $1"
    echo "tests cmd $2"
    echo "find results are"
    echo $3
    echo "custome find results are"
    echo "find results are"
    echo $4
    let failedTests=$failedTests+1
    echo "----------------------"
}

function RunTest
{
    cmd=$1
    functionName=$2
    findResult=`find $cmd > $findResultFile`
    customeFindResult=`$customeFindPath $cmd > $customeFindResultFile`
    
    if [[ $customeFindResult -ne $findResult ]]
    then
        PrintError $functionName $cmd $findResult $customeFindResult
    fi

    if [[ $customeFindResult -ne 0 ]]
    then
        # if both fail it should not print the same error massage 
        return 0
    fi

    if [[ `diff $findResultFile $customeFindResultFile | wc -l` -gt 0 ]]
    then
        PrintError $functionName `cat $findResultFile` `cat $customeFindResultFile`
    fi
}


function MakeTestsFiles
{
    mkdir $testsFolder
    cd $testsFolder
    mkdir -p a/b/c/
    for i in `seq 6` 
    do
        touch a/$i.txt
        touch a/b/$i.txt
    done
    touch a/b/c/100.txt
    touch a/b/101.txt
}

function RemoveTestFiles
{
    rm -rf $testsFolder
    rm $customeFindResultFile $findResultFile
}

function DepthMax2
{
    RunTest "-maxdepth 2 -name *.txt" DepthMax2
}

function DepthMinMax3
{
    RunTest "-maxdepth 3 -mindepth 3 -name *.txt" DepthMinMax3
}

function DepthMin1
{
    RunTest "-mindepth 1 -name *.txt" DepthMin1
}

function BadParams
{
    RunTest "-mindepth -1 -name a" BadParams
}

function BadParams2
{
    RunTest "-maxdepth -1 -name a" BadParams2
}

function BadParams3
{
    RunTest "-name a -maxdepth -1" BadParams3
}

function RunFromDifferantPath
{
    RunTest "b -name *.txt" RunFromDifferantPath
}

MakeTestsFiles
DepthMax2
DepthMinMax3
DepthMin1
BadParams
BadParams2
BadParams3
RunFromDifferantPath
if [[ $failedTests -eq 0 ]]
then
    echo "all tests passed :)"
else
    echo "failed in $failedTests tests"
fi
RemoveTestFiles